#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <list>
#include "tree.h"
using namespace std;

void main()
{
	Tree<string> mytree;
	list<int> txt_line;
	ifstream is("mytext.txt");
	string line, word;
	int line_num = 1;
	list<Node<string>> line_list;
	while(getline(is, line)){
		istringstream iss(line);
		while(iss){
			iss >> word;
			mytree.insert(word, line_num);
		}
		line_num++;
	}
	is.close();
	mytree.inorder();
	int d_line;
	cout << "Enter line number: ";cin >> d_line;
	mytree.search_line(d_line);
	
}